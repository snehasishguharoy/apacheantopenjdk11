package com.example.ant;
import javax.xml.bind.annotation.XmlAttribute;
public class PhoneNumber {
	
	private String type;
    private String value;
    private Boolean defaultNumber;
    @XmlAttribute
    public String getType() {
        return type;
    }
 
    public void setType(String type) {
        this.type = type;
    }
 
    @Override
	public String toString() {
		return "PhoneNumber [type=" + type + ", value=" + value + ", defaultNumber=" + defaultNumber + "]";
	}

	@XmlAttribute
    public String getValue() {
        return value;
    }
 
    public void setValue(String value) {
        this.value = value;
    }
    
    @XmlAttribute
    public Boolean isDefaultNumber() {
        return defaultNumber;
    }
 
    public void setDefaultNumber(Boolean defaultNumber) {
        this.defaultNumber = defaultNumber;
    }

}
